﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Tamago.Unity.Utilities;

public class SoundManagerTest : MonoBehaviour {

	public List<AudioClip> Clip = new List<AudioClip>();

	SoundManager manager;

	// Use this for initialization
	void Start () {
		manager = gameObject.AddComponent<SoundManager>();
		manager.GeneratePlayer(SoundPlayerName.SE1, 1);
		manager.ChangeMaxPlayNum(SoundPlayerName.SE1,2);
		var audio = Resources.Load<AudioClip>("Sound/sound");
		manager.AddItem(SoundName.Sound, audio);
		StartCoroutine("Play");
	}

	IEnumerator Play(){
		manager.Play(SoundPlayerName.SE1, SoundName.Sound); 
		yield return new WaitForSeconds(0.5f);
		manager.Play(SoundPlayerName.SE1, SoundName.Sound);
		manager.SetVolume(SoundPlayerName.SE1, 0.5f);
		yield return new WaitForSeconds(0.5f);
		manager.Play(SoundPlayerName.SE1, SoundName.Sound);
		yield return new WaitForSeconds(4);
		manager.Play(SoundPlayerName.SE1, SoundName.Sound);
		yield return new WaitForSeconds(1);
		manager.Stop(SoundPlayerName.SE1);
	}

	enum SoundPlayerName{
		SE1
	}

	enum SoundName{
		Sound
	}

}
