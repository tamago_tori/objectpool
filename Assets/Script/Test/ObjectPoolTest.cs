﻿using UnityEngine;
using System.Collections;
using com.tamagokobo.Unity.Utilities;

public class ObjectPoolTest : MonoBehaviour {

	enum Hoge{Hoge};

	// Use this for initialization
	void Start () {
        var objectPool = new ObjectPool<GameObject>();

        var item1 = new GameObject();
        objectPool.AddItem(item1);
        var item2 = new GameObject();
        objectPool.AddItem(item2);
        var item3 = new GameObject();
        objectPool.AddItem(item3);


        Debug.Log(objectPool.GetItem());
        Debug.Log(objectPool.GetItem());
        Debug.Log(objectPool.GetItem());
        Debug.Log(objectPool.GetItem());

        objectPool.AddItem(item1);

        Debug.Log(objectPool.GetItem());
        Debug.Log(objectPool.GetItem());

        objectPool.AddItem(item1);
        objectPool.AddItem(item2);
        objectPool.AddItem(item3);

        Debug.Log(objectPool.GetItemAll().Count);
    }

}
