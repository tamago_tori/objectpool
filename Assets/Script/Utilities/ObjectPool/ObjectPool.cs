using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace com.tamagokobo.Unity.Utilities{
	public class ObjectPool<T> where T: class{

		List<T> m_poolList = new List<T>();

        /// <summary>
        /// アイテムを追加
        /// </summary>
        /// <param name="addItem"></param>
		public void AddItem(T addItem) {
			m_poolList.Add (addItem);
		}

		/// <summary>
        /// Listでアイテムを追加
        /// </summary>
        /// <param name="addList"></param>
		public void AddItemList(List<T> addList) {
			addList.ForEach(item => {
				AddItem(item);
			});
		}

		/// <summary>
        /// プールオブジェクトを取得
        /// </summary>
        /// <returns></returns>
		public T GetItem() {
			if(m_poolList.Count == 0)
                return null;

			var item = m_poolList[0] as T;
            if (item == null)
                return null;

			m_poolList.RemoveAt(0);

            return item;
		}

		/// <summary>
        /// プールオブジェクトをListで取得
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="getNum"></param>
        /// <returns></returns>
		public List<T> GetItemList(int getNum) {
			var list = new List<T>();

			for(var i=0;i<getNum;i++){
				var item = GetItem();
                if (item == null)
                    break;

				list.Add (item);
			}

			return list;
		}

        public List<T> GetItemAll() {
            var count = Count();

            return GetItemList(count);
        }

        public int Count() {
            return m_poolList.Count;
        }

        /// <summary>
        /// 指定のプールアイテムを解放
        /// </summary>
        /// <param name="releaseItem"></param>
        public void ReleaseItem(T releaseItem) {
            m_poolList.Remove(releaseItem);
        }

        /// <summary>
        /// プールを全解放
        /// </summary>
        public void ReleaseAll() {
            m_poolList.Clear();
        }


    }
}

