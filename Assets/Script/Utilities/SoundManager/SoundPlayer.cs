﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Tamago.Unity.Utilities;

namespace Tamago.Unity.Utilities{
	public class SoundPlayer : MonoBehaviour {

		List<AudioSource> sourceList = new List<AudioSource>();
		public float Volume = 1;
		public bool Loop = false;
		List<AudioSource> stopList = new List<AudioSource>();
		List<AudioSource> playingList = new List<AudioSource>();

		public void Initialize(int maxPlayNum, float volume=1, bool loop=false){
			ChangeMaxPlayNum(maxPlayNum);
			Volume = volume;
			Loop = loop;
		}

		public void ChangeMaxPlayNum(int maxPlayNum){
			var diff = maxPlayNum - sourceList.Count;
			if(diff > 0){
				AddAudioSource(diff);
				return;
			}
			if(diff < 0){
				RemoveAudioSource(-diff);
				return;
			}
		}

		public void Play(AudioClip audioClip, float volume = 1, float delay = 0){
			UpdateSourceList();
			if(stopList.Count == 0){
				return;
			}
			var player = stopList[0];
			player.clip = audioClip;
			player.volume = Volume * volume;
			player.PlayDelayed(delay);

		}

		public void Stop(){
			UpdateSourceList();
			playingList.ForEach((source) => {
				source.Stop();
			});
		}

		public void Pause(){
			UpdateSourceList();
			playingList.ForEach((source) => {
				source.Pause();
			});
		}

		public void SetVolume(float volume){
			Volume = volume;
			sourceList.ForEach((source) => {
				source.volume = Volume;
			});
		}

		public void SetLoop(bool loop){
			Loop = loop;
			sourceList.ForEach((source) => {
				source.loop = Loop;
			});
		}
			
		void AddAudioSource(int num){
			int i;
			for(i=0;i<num;i++){
				var source = gameObject.AddComponent<AudioSource>();
				source.playOnAwake = false;
				source.loop = Loop;
				source.volume = Volume;
				sourceList.Add (source);
			}
		}

		void RemoveAudioSource(int num){
			var removeCount = 0;
			UpdateSourceList();
			stopList.ForEach((source) => {
				if(++removeCount > num){
					return;
				}
				sourceList.Remove(source);
				Destroy(source);
			});
			playingList.ForEach((source) => {
				if(++removeCount > num){
					return;
				}
				sourceList.Remove(source);
				Destroy(source);
			});
		}

		void UpdateSourceList(){
			playingList.Clear();
			stopList.Clear();

			sourceList.ForEach((source) => {
				if(source.isPlaying){
					playingList.Add (source);
				} else {
					stopList.Add (source);
				}
			});
		}
	}
}
