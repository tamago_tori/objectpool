﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Tamago.Unity.Utilities;

namespace Tamago.Unity.Utilities{
	public class SoundManager : MonoBehaviour {
		
		Dictionary<Enum,AudioClip> audioPool = new Dictionary<Enum, AudioClip>();
		public List<AudioClip> PublicAudioClipList = new List<AudioClip>();
		//public List<AudioSource> PublicAudioSourceList = new List<AudioSource>();

		Dictionary<Enum, SoundPlayer> soundPlayerDictionary = new Dictionary<Enum, SoundPlayer>();

		public void AddItem(Enum key, AudioClip audioClip){
			audioPool[key] = audioClip;
		}

		public SoundPlayer GeneratePlayer(Enum key, int maxPlayNum, float volume = 1){
			var player = new GameObject("SoundPlayer").AddComponent<SoundPlayer>();
			player.transform.parent = transform;
			player.Initialize(maxPlayNum, volume);
			soundPlayerDictionary.Add (key, player);
			return player;
		}

		public SoundPlayer GetPlayer(Enum key){
			return soundPlayerDictionary[key];
		}

		public void Play(Enum playerKey, Enum soundKey, float volume=1, float delay=0){
			var player = soundPlayerDictionary[playerKey];
			var audio = audioPool[soundKey];
			if(audio == null){
				return;
			}
			player.Play(audio,volume, delay);
		}

		public void Stop(Enum playerKey){
			var player = soundPlayerDictionary[playerKey];
			player.Stop();
		}

		public void StopAll(){
			foreach (var pair in soundPlayerDictionary){
				Stop (pair.Key);
			}
		}

		public void Pause(Enum playerKey){
			var player = soundPlayerDictionary[playerKey];
			player.Pause();
		}

		public void PauseAll(){
			foreach (var pair in soundPlayerDictionary){
				Pause (pair.Key);
			}
		}

		public void ChangeMaxPlayNum(Enum playerKey, int num){
			var player = soundPlayerDictionary[playerKey];
			player.ChangeMaxPlayNum(num);
		}

		public void SetVolume(Enum playerKey, float volume){
			var player = soundPlayerDictionary[playerKey];
			player.SetVolume(volume);
		}

		public void SetVolumeAll(float volume){
			foreach(var pair in soundPlayerDictionary){
				SetVolume(pair.Key, volume);
			}
		}

		public void SetLoop(Enum playerKey, bool loop){
			var player = soundPlayerDictionary[playerKey];
			player.SetLoop(loop);
		}
	}
}


